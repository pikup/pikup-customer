import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:pikup_customer/data/models/phone_number.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:authentication_repository/authentication_repository.dart';
import 'package:pikup_customer/data/models/sms_code.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final AuthenticationRepository _authenticationRepository;

  LoginCubit(this._authenticationRepository) : super(const LoginState());

  void phoneNumberChanged(String value) {
    final phoneNumber = PhoneNumber.dirty(value);
    emit(state.copyWith(
      phoneNumber: phoneNumber,
      phoneNumberFormStatus: Formz.validate([phoneNumber]),
    ));
  }

  void smsCodeChanged(String value) {
    final smsCode = SmsCode.dirty(value);
    emit(state.copyWith(
      smsCode: smsCode,
      smsCodeFormStatus: Formz.validate([smsCode]),
    ));
  }

  Future<void> verifyPhoneNumber() async {
    if (!state.phoneNumberFormStatus.isValidated) return;
    emit(state.copyWith(
        phoneNumberFormStatus: FormzStatus.submissionInProgress));
    try {
      await _authenticationRepository.verifyPhoneNumber(
        phoneNumber: state.phoneNumber.value,
        verificationCompleted: onPhoneVerificationCompleted,
        verificationFailed: _onPhoneVerificationFailed,
        codeSent: onCodeSent,
        codeAutoRetrievalTimeout: onCodeAutoRetrievalTimeout,
      );

      emit(
          state.copyWith(phoneNumberFormStatus: FormzStatus.submissionSuccess));
    } on Exception {
      emit(
          state.copyWith(phoneNumberFormStatus: FormzStatus.submissionFailure));
    }
  }

  /// This handler will only be called on Android devices
  /// which support automatic SMS code resolution.
  void onPhoneVerificationCompleted(PhoneAuthCredential credential) async {
    emit(state.copyWith(smsCodeFormStatus: FormzStatus.submissionInProgress));
    try {
      await _authenticationRepository.signInWithCredential(credential);
      emit(state.copyWith(smsCodeFormStatus: FormzStatus.submissionSuccess));
    } on Exception {
      emit(state.copyWith(smsCodeFormStatus: FormzStatus.submissionFailure));
    }
  }

  /// When Firebase sends an SMS code to the device,
  /// this handler is triggered with a [verificationId] and [resendToken]
  void onCodeSent(String verificationId, int? resendToken) {
    emit(
      state.copyWith(
        verificationState: LoginVerificationState(
          verificationStatus: LoginVerificationStatus.codeSent,
        ),
        verificationId: verificationId,
      ),
    );
  }

  /// On Android devices which support automatic SMS code resolution,
  /// this handler will be called if the device has not automatically resolved
  /// an SMS message within a certain time frame. Once the time frame has passed,
  /// the device will no longer attempt to resolve any incoming messages.
  void onCodeAutoRetrievalTimeout(String verificationId) {
    // ToDo: Implement this method
  }

  /// Sign the user in (or link) with the credential
  Future<void> signInWithSmsCode() async {
    if (state.verificationId == null && state.smsCodeFormStatus.isValidated) {
      return;
    }

    emit(state.copyWith(smsCodeFormStatus: FormzStatus.submissionInProgress));
    try {
      await _authenticationRepository.signInWithSmsCode(
        verificationId: state.verificationId!,
        smsCode: state.smsCode.value,
      );
      emit(state.copyWith(smsCodeFormStatus: FormzStatus.submissionSuccess));
    } on Exception {
      emit(state.copyWith(smsCodeFormStatus: FormzStatus.submissionFailure));
    }
  }

  /// This method will be called when firebase returns an error
  /// for example for an incorrect phone number
  /// or if the SMS quota for the project has exceeded
  void _onPhoneVerificationFailed(FirebaseAuthException exception) {
    emit(
      state.copyWith(
        verificationState: LoginVerificationState(
          verificationStatus: LoginVerificationStatus.Failed,
          message: exception.message,
        ),
      ),
    );
  }
}
