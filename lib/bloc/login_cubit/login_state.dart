part of 'login_cubit.dart';

enum LoginVerificationStatus {
  unknown,
  Failed,
  codeSent,
}

class LoginVerificationState extends Equatable {
  final LoginVerificationStatus verificationStatus;
  final String? message;

  const LoginVerificationState({
    this.verificationStatus = LoginVerificationStatus.unknown,
    this.message,
  });

  @override
  List<Object?> get props => [verificationStatus, message];
}

class LoginState extends Equatable {
  final PhoneNumber phoneNumber;
  final SmsCode smsCode;
  final FormzStatus phoneNumberFormStatus;
  final FormzStatus smsCodeFormStatus;
  final LoginVerificationState loginVerificationStatus;
  final String? verificationId;

  const LoginState({
    this.phoneNumber = const PhoneNumber.pure(),
    this.smsCode = const SmsCode.pure(),
    this.phoneNumberFormStatus = FormzStatus.pure,
    this.smsCodeFormStatus = FormzStatus.pure,
    this.loginVerificationStatus = const LoginVerificationState(),
    this.verificationId,
  });

  LoginState copyWith({
    PhoneNumber? phoneNumber,
    SmsCode? smsCode,
    FormzStatus? phoneNumberFormStatus,
    FormzStatus? smsCodeFormStatus,
    LoginVerificationState? verificationState,
    String? verificationId,
  }) {
    return LoginState(
      phoneNumber: phoneNumber ?? this.phoneNumber,
      smsCode: smsCode ?? this.smsCode,
      phoneNumberFormStatus:
          phoneNumberFormStatus ?? FormzStatus.pure,
      smsCodeFormStatus: smsCodeFormStatus ?? FormzStatus.pure,
      loginVerificationStatus:
          verificationState ?? const LoginVerificationState(),
      verificationId: verificationId ?? this.verificationId,
    );
  }

  @override
  List<Object?> get props => [
        phoneNumber,
        smsCode,
        phoneNumberFormStatus,
        smsCodeFormStatus,
        loginVerificationStatus,
        verificationId,
      ];
}
