part of 'user_info_cubit.dart';

class UserInfoState extends Equatable {
  final UserName userName;
  final Email email;
  final FormzStatus userNameFormStatus;
  final FormzStatus emailFormStatus;

  const UserInfoState({
    this.userName = const UserName.pure(),
    this.email = const Email.pure(),
    this.userNameFormStatus = FormzStatus.pure,
    this.emailFormStatus = FormzStatus.valid,
  });

  UserInfoState copyWith({
    UserName? userName,
    Email? email,
    FormzStatus? userNameFormStatus,
    FormzStatus? emailFormStatus,
  }) {
    return UserInfoState(
      userName: userName ?? this.userName,
      email: email ?? this.email,
      userNameFormStatus: userNameFormStatus ?? this.userNameFormStatus,
      emailFormStatus: emailFormStatus ?? this.emailFormStatus,
    );
  }

  @override
  List<Object?> get props => [userName, email, userNameFormStatus, emailFormStatus];
}