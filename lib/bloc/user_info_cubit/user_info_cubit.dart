import 'package:authentication_repository/authentication_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firestore_repository/firestore_repository.dart';
import 'package:formz/formz.dart';
import 'package:pikup_customer/data/models/email.dart';
import 'package:pikup_customer/data/models/user_name.dart';

part 'user_info_state.dart';

class UserInfoCubit extends Cubit<UserInfoState> {
  final AuthenticationRepository _authenticationRepository;
  final FirestoreUserRepository _userRepository;

  UserInfoCubit(this._authenticationRepository, this._userRepository)
      : super(const UserInfoState());

  /// This method is called when user types into the name form field
  void userNameChanged(String value) {
    final userName = UserName.dirty(value);
    emit(state.copyWith(
      userName: userName,
      userNameFormStatus: Formz.validate([userName]),
    ));
  }

  /// This method is called when user types into the email form field
  void emailChanged(String value) {
    final email = Email.dirty(value);
    emit(state.copyWith(
      email: email,
      emailFormStatus: Formz.validate([email]),
    ));
  }

  /// This method is responsible for updating user data to the firebase
  Future<void> updateUserInfo() async {
    if (!state.userNameFormStatus.isValidated ||
        !state.emailFormStatus.isValidated) return;

    emit(
      state.copyWith(
        userNameFormStatus: FormzStatus.submissionInProgress,
        emailFormStatus: FormzStatus.submissionInProgress,
      ),
    );

    try {
      await _authenticationRepository.updateUserInfo(
        displayName: state.userName.value.trim(),
        email: state.email.value.isEmpty ? null : state.email.value.trim(),
      );

      /// Add Customer info to firestore customer collection
      final User user = await _authenticationRepository.getCurrentUser();
      if (user != User.empty) {
        await _userRepository.addCustomer(
          id: user.uid,
          phoneNumber: user.phoneNumber,
          name: state.userName.value.trim(),
          email: state.email.value.isEmpty ? null : state.email.value.trim(),
          isEmailVerified: false,
        );
      }

      emit(
        state.copyWith(
          userNameFormStatus: FormzStatus.submissionSuccess,
          emailFormStatus: FormzStatus.submissionSuccess,
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          userNameFormStatus: FormzStatus.submissionFailure,
          emailFormStatus: FormzStatus.submissionFailure,
        ),
      );
    }
  }
}
