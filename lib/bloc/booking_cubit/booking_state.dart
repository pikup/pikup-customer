part of 'booking_cubit.dart';

enum BookingEvent {
  unknown,
  loading,
  currentLocationUpdated,
  pickupDestinationLocationUpdated,
  currentLocationAddressUpdated,
  pickupLocationAddressUpdated,
  destinationLocationAddressUpdated,
  pickupAddressPredictionsUpdated,
  destinationAddressPredictionsUpdated,
  pickupDestinationLocationUpdateError,
  currentLocationFetchError,
  clearedPredictions,
  directionDetailsUpdated,
  drawPolyline,
}

class BookingState extends Equatable {
  final LatLngPosition? currentLocation;
  final FormattedAddress currentLocationAddress;

  final LatLngPosition? pickupLocation;
  final FormattedAddress pickupLocationAddress;

  final LatLngPosition? destinationLocation;
  final FormattedAddress destinationLocationAddress;

  final List<FormattedAddress> pickupAddressPredictions;
  final List<FormattedAddress> destinationAddressPredictions;

  final DirectionDetails? directionDetails;

  final Set<Polyline> polylineSet;
  final Set<Marker> markerSet;
  final Set<Circle> circleSet;

  final BookingEvent event;

  const BookingState({
    this.currentLocation,
    this.currentLocationAddress = FormattedAddress.empty,
    this.pickupLocation,
    this.pickupLocationAddress = FormattedAddress.empty,
    this.destinationLocation,
    this.destinationLocationAddress = FormattedAddress.empty,
    this.pickupAddressPredictions = const <FormattedAddress>[],
    this.destinationAddressPredictions = const <FormattedAddress>[],
    this.directionDetails,
    this.polylineSet = const <Polyline>{},
    this.markerSet = const <Marker>{},
    this.circleSet = const <Circle>{},
    this.event = BookingEvent.unknown,
  });

  BookingState copyWith({
    LatLngPosition? currentLocation,
    FormattedAddress? currentLocationAddress,
    LatLngPosition? pickupLocation,
    FormattedAddress? pickupLocationAddress,
    LatLngPosition? destinationLocation,
    FormattedAddress? destinationLocationAddress,
    List<FormattedAddress>? pickupAddressPredictions,
    List<FormattedAddress>? destinationAddressPredictions,
    DirectionDetails? directionDetails,
    Set<Polyline>? polylineSet,
    Set<Marker>? markerSet,
    Set<Circle>? circleSet,
    BookingEvent? event,
  }) {
    return BookingState(
      currentLocation: currentLocation ?? this.currentLocation,
      currentLocationAddress:
          currentLocationAddress ?? this.currentLocationAddress,
      pickupLocation: pickupLocation ?? this.pickupLocation,
      pickupLocationAddress:
          pickupLocationAddress ?? this.pickupLocationAddress,
      destinationLocation: destinationLocation ?? this.destinationLocation,
      destinationLocationAddress:
          destinationLocationAddress ?? this.destinationLocationAddress,
      pickupAddressPredictions:
          pickupAddressPredictions ?? this.pickupAddressPredictions,
      destinationAddressPredictions:
          destinationAddressPredictions ?? this.destinationAddressPredictions,
      directionDetails: directionDetails ?? this.directionDetails,
      polylineSet: polylineSet ?? this.polylineSet,
      markerSet: markerSet ?? this.markerSet,
      circleSet: circleSet ?? this.circleSet,
      event: event ?? BookingEvent.unknown,
    );
  }

  @override
  List<Object?> get props => [
        currentLocation,
        currentLocationAddress,
        pickupLocation,
        pickupLocationAddress,
        destinationLocation,
        destinationLocationAddress,
        pickupAddressPredictions,
        destinationAddressPredictions,
        directionDetails,
        polylineSet,
        markerSet,
        circleSet,
        event,
      ];
}
