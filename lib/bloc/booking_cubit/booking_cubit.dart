import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';
import 'package:rest_api_repository/geocoding_repository.dart';
import 'package:rest_api_repository/places_repository.dart';
import 'package:rest_api_repository/directions_repository.dart';
import 'package:geolocator/geolocator.dart';

part 'booking_state.dart';

class BookingCubit extends Cubit<BookingState> {
  final _geocodingRepository = GeocodingRepository();
  final _placesRepository = PlacesRepository();
  final _directionsRepository = DirectionsRepository();

  BookingCubit() : super(const BookingState());

  Future<void> setCurrentPosition() async {
    try {
      await Geolocator.requestPermission();
      final permission = await Geolocator.checkPermission();

      if (permission == LocationPermission.deniedForever) {
        emit(
          state.copyWith(
            event: BookingEvent.currentLocationFetchError,
          ),
        );
      }

      if (permission == LocationPermission.always ||
          permission == LocationPermission.whileInUse) {
        final position = await Geolocator.getCurrentPosition();
        emit(
          state.copyWith(
            currentLocation: LatLngPosition(
              latitude: position.latitude,
              longitude: position.longitude,
            ),
            event: BookingEvent.currentLocationUpdated,
          ),
        );
      }

      await getCurrentLocationAddress();
    } on Exception {
      emit(
        state.copyWith(
          event: BookingEvent.currentLocationFetchError,
        ),
      );
    }
  }

  Future<void> getCurrentLocationAddress() async {
    if (state.currentLocation != null) {
      try {
        final currentAddress = await _geocodingRepository.reverseGeocode(
          latitude: state.currentLocation!.latitude,
          longitude: state.currentLocation!.longitude,
        );

        emit(
          state.copyWith(
            currentLocationAddress: currentAddress,
            pickupLocationAddress: currentAddress,
            event: BookingEvent.currentLocationAddressUpdated,
          ),
        );
      } on Exception {
        print('Error: Unable to fetch current formatted address.');
      }
    }
  }

  Future<void> getPickupAddressPredictions(String val) async {
    if (val.isEmpty) {
      emit(
        state.copyWith(
          pickupAddressPredictions: const <FormattedAddress>[],
          pickupLocationAddress: null,
          event: BookingEvent.pickupAddressPredictionsUpdated,
        ),
      );
    } else {
      try {
        final placesPredictions = await _placesRepository.autoComplete(
          input: val,
        );

        emit(
          state.copyWith(
            pickupAddressPredictions: placesPredictions,
            event: BookingEvent.pickupAddressPredictionsUpdated,
          ),
        );
      } on Exception {
        print('Error: Unable to fetch places predictions.');
      }
    }
  }

  Future<void> getDestinationAddressPredictions(String val) async {
    if (val.isEmpty) {
      emit(
        state.copyWith(
          destinationAddressPredictions: const <FormattedAddress>[],
          destinationLocationAddress: null,
          event: BookingEvent.destinationAddressPredictionsUpdated,
        ),
      );
    } else {
      try {
        final placesPredictions = await _placesRepository.autoComplete(
          input: val,
        );

        emit(
          state.copyWith(
            destinationAddressPredictions: placesPredictions,
            event: BookingEvent.destinationAddressPredictionsUpdated,
          ),
        );
      } on Exception {
        print('Error: Unable to fetch places predictions.');
      }
    }
  }

  void selectPickupAddress(FormattedAddress address) {
    emit(
      state.copyWith(
        pickupLocationAddress: address,
        pickupAddressPredictions: const <FormattedAddress>[],
        event: BookingEvent.pickupLocationAddressUpdated,
      ),
    );
  }

  void selectDestinationAddress(FormattedAddress address) {
    emit(
      state.copyWith(
        destinationLocationAddress: address,
        destinationAddressPredictions: const <FormattedAddress>[],
        event: BookingEvent.destinationLocationAddressUpdated,
      ),
    );
  }

  Future<void> getPickupDestinationPositions() async {
    if (state.pickupLocationAddress.isEmpty() ||
        state.destinationLocationAddress.isEmpty()) {
      return;
    }

    emit(
      state.copyWith(event: BookingEvent.loading),
    );

    try {
      final pickupPosition = await _placesRepository.getPlacePosition(
        placeId: state.pickupLocationAddress.placeId,
      );

      final destinationPosition = await _placesRepository.getPlacePosition(
        placeId: state.destinationLocationAddress.placeId,
      );

      emit(
        state.copyWith(
          pickupLocation: pickupPosition,
          destinationLocation: destinationPosition,
          event: BookingEvent.pickupDestinationLocationUpdated,
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          event: BookingEvent.pickupDestinationLocationUpdateError,
        ),
      );
      print('Unable to fetch address position details');
    }
  }

  void clearPredictions() {
    emit(
      state.copyWith(
        pickupAddressPredictions: const <FormattedAddress>[],
        destinationAddressPredictions: const <FormattedAddress>[],
        event: BookingEvent.clearedPredictions,
      ),
    );
  }

  Future<void> getDirectionDetails() async {
    if (state.pickupLocation == null || state.destinationLocation == null) {
      return;
    }

    final directionDetails = await _directionsRepository.getDirectionDetails(
      initialPosition: state.pickupLocation!,
      finalPosition: state.destinationLocation!,
    );

    emit(state.copyWith(
      directionDetails: directionDetails,
      event: BookingEvent.directionDetailsUpdated,
    ));
  }

  void decodePolylinePoints() {
    final polylinePoints = PolylinePoints();
    final decodedPolylinePoints =
        polylinePoints.decodePolyline(state.directionDetails!.encodedPoints);

    List<LatLng> polylineCoordinates = [];
    if (decodedPolylinePoints.isNotEmpty) {
      decodedPolylinePoints.forEach(
        (pointLatLng) {
          polylineCoordinates.add(
            LatLng(
              pointLatLng.latitude,
              pointLatLng.longitude,
            ),
          );
        },
      );
    }

    final polyline = Polyline(
      polylineId: PolylineId('polylineId'),
      color: PikupColors.primaryColor,
      jointType: JointType.round,
      points: polylineCoordinates,
      width: 8,
      geodesic: true,
      startCap: Cap.roundCap,
      endCap: Cap.roundCap,
    );

    emit(
      state.copyWith(
        polylineSet: { polyline },
        event: BookingEvent.drawPolyline,
      ),
    );
  }

  void updateMarkerSet(Set<Marker> markers) {
    emit(
      state.copyWith(
        markerSet: markers,
      )
    );
  }

  void updateCircleSet(Set<Circle> circles) {
    emit(
        state.copyWith(
          circleSet: circles,
        )
    );
  }
}
