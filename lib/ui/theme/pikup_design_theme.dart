import 'package:flutter/material.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';

class PikupDesignTheme {

  static Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return PikupColors.deepYellow;
    }
    return PikupColors.primaryColor;
  }

  static ThemeData get lightTheme {
    return ThemeData(
      primaryColor: PikupColors.primaryColor,
      secondaryHeaderColor: PikupColors.deepYellow,
      scaffoldBackgroundColor: Colors.white,
      fontFamily: 'Montserrat',
      textTheme: ThemeData.light().textTheme,

      appBarTheme: AppBarTheme(
        centerTitle: true,
        brightness: Brightness.dark,
      ),

      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith(getColor),
        ),
      ),

      floatingActionButtonTheme: FloatingActionButtonThemeData(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        backgroundColor: PikupColors.primaryColor,
        splashColor: PikupColors.deepYellow,
      ),

      iconTheme: IconThemeData(
        color: PikupColors.deepYellow,
      )
    );
  }
}