import 'dart:ui';
import 'package:flutter/material.dart';

class PikupColors {
  static final  primaryColor = Colors.black;
  static final deepYellow = Color(0xFFF9A135);
  static final grey = Colors.grey.shade600;
}