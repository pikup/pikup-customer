import 'package:flutter/material.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';

class SplashScreen extends StatelessWidget {
  static const route = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(
          backgroundColor: PikupColors.deepYellow,
        ),
      ),
      backgroundColor: PikupColors.primaryColor,
    );
  }
}
