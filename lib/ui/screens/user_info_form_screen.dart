import 'package:authentication_repository/authentication_repository.dart';
import 'package:firestore_repository/firestore_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:pikup_customer/bloc/user_info_cubit/user_info_cubit.dart';
import 'package:pikup_customer/constants/constants.dart';
import 'package:pikup_customer/ui/widgets/loading_progress.dart';
import 'package:pikup_customer/ui/widgets/stretched_button.dart';
import 'package:pikup_customer/ui/widgets/text_form_field_widget.dart';
import 'package:pikup_customer/utility/util.dart';
import 'package:provider/provider.dart';

class UserInfoFormScreen extends StatelessWidget {
  static const String routeName = '/user_info_form_screen';

  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserInfoCubit>(
      create: (_) => UserInfoCubit(
        context.read<AuthenticationRepository>(),
        context.read<FirestoreUserRepository>(),
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 30,
              vertical: 10,
            ),
            child: UserInfoForm(),
          ),
        ),
      ),
    );
  }
}

class UserInfoForm extends StatefulWidget {
  @override
  _UserInfoFormState createState() => _UserInfoFormState();
}

class _UserInfoFormState extends State<UserInfoForm> {
  BuildContext? _dialogContext;

  @override
  Widget build(BuildContext context) {
    return BlocListener<UserInfoCubit, UserInfoState>(
      listener: _onNewEvent,
      child: Center(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Text(
              'Enter these info for hassle free service.',
              style: TextStyle(
                fontSize: Util.displayHeight(context) * 0.035,
                fontWeight: Theme.of(context).textTheme.headline6?.fontWeight,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            BlocBuilder<UserInfoCubit, UserInfoState>(
              buildWhen: (previous, current) =>
                  previous.userName != current.userName,
              builder: (context, state) {
                return TextFormFieldWidget(
                  prefixIcon: Icon(Icons.account_circle),
                  labelText: 'Enter your name',
                  keyboardType: TextInputType.name,
                  onChanged: (val) =>
                      context.read<UserInfoCubit>().userNameChanged(val),
                  errorText:
                      state.userName.invalid ? 'Name cannot be empty.' : null,
                );
              },
            ),
            const SizedBox(
              height: 20,
            ),
            BlocBuilder<UserInfoCubit, UserInfoState>(
              buildWhen: (previous, current) => previous.email != current.email,
              builder: (context, state) {
                return Container(
                  child: TextFormFieldWidget(
                    prefixIcon: Icon(Icons.email),
                    labelText: 'Enter your email (optional)',
                    keyboardType: TextInputType.emailAddress,
                    onChanged: (val) =>
                        context.read<UserInfoCubit>().emailChanged(val),
                    errorText: state.email.invalid ? 'Invalid email.' : null,
                  ),
                );
              },
            ),
            const SizedBox(
              height: 20,
            ),
            StretchedButton(
              text: Text('Submit'),
              onPressed: () {
                final userInfoCubit = context.read<UserInfoCubit>();
                userInfoCubit.updateUserInfo();
              },
            ),
          ],
        ),
      ),
    );
  }

  /// Listens to the events coming from [UserInfoCubit]
  void _onNewEvent(BuildContext context, UserInfoState state) {
    if (state.userNameFormStatus == FormzStatus.submissionInProgress) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (buildContext) {
          _dialogContext = buildContext;
          return LoadingProgress();
        },
      );
    } else if (state.userNameFormStatus == FormzStatus.submissionFailure) {
      if (_dialogContext != null) {
        Navigator.of(_dialogContext!).pop();
        _dialogContext = null;
      }

      ScaffoldMessenger.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            duration: Duration(
              seconds: kSnackBarDuration,
            ),
            content: Text('Something went wrong.'),
            action: SnackBarAction(
              label: 'Dismiss',
              onPressed: () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
              },
            ),
          ),
        );
    }
  }
}
