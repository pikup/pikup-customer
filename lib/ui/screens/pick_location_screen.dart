import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pikup_customer/constants/string_resource.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';
import 'package:pikup_customer/ui/widgets/search_field.dart';
import 'package:pikup_customer/ui/widgets/stretched_button.dart';
import 'package:provider/provider.dart';
import 'package:pikup_customer/bloc/booking_cubit/booking_cubit.dart';
import 'package:rest_api_repository/geocoding_repository.dart';

class PickLocationScreen extends StatelessWidget {
  static const String routeName = '/search_location_screen';

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        FocusScope.of(context).unfocus();
        final bookingCubit = Provider.of<BookingCubit>(context, listen: false);
        bookingCubit.clearPredictions();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(StringResource.ADD_LOCATION_SCREEN_TITLE),
        ),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.all(8.0),
            shrinkWrap: true,
            children: <Widget>[
              Card(
                elevation: 10,
                child: Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Column(
                    children: [
                      LocationInputWidget(),
                      Divider(),
                      StretchedButton(
                        text: Text(StringResource.CONTINUE_BUTTON_TITLE),
                        backgroundColor: PikupColors.primaryColor,
                        onPressed: () async {
                          await _onContinueButtonPressed(context);
                        },
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 80),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _onContinueButtonPressed(BuildContext context) async {
    Navigator.of(context).pop();
    final bookingCubit = Provider.of<BookingCubit>(context, listen: false);
    await bookingCubit.getPickupDestinationPositions();
    if (bookingCubit.state.pickupLocation == null ||
        bookingCubit.state.destinationLocation == null) {
      return;
    }
    await bookingCubit.getDirectionDetails();
  }
}

class LocationInputWidget extends StatefulWidget {
  @override
  _LocationInputWidgetState createState() => _LocationInputWidgetState();
}

class _LocationInputWidgetState extends State<LocationInputWidget> {
  final _pickupTextController = TextEditingController();
  final _destinationTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _initializePickupTextField();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BookingCubit, BookingState>(
      builder: (context, state) {
        return Column(
          children: <Widget>[
            BlocBuilder<BookingCubit, BookingState>(
              buildWhen: (previous, current) {
                return (current.event ==
                        BookingEvent.pickupLocationAddressUpdated ||
                    current.event ==
                        BookingEvent.pickupAddressPredictionsUpdated);
              },
              builder: (context, snapshot) {
                return SearchField(
                  controller: _pickupTextController,
                  predictions: Provider.of<BookingCubit>(context)
                      .state
                      .pickupAddressPredictions,
                  onChanged: (val) {
                    final bookingCubit =
                        Provider.of<BookingCubit>(context, listen: false);
                    bookingCubit.getPickupAddressPredictions(val);
                  },
                  onItemSelected: (FormattedAddress val) {
                    FocusScope.of(context).unfocus();
                    Provider.of<BookingCubit>(context, listen: false)
                        .selectPickupAddress(val);
                  },
                  hintText: 'Pickup location',
                  prefixIcon: Icon(
                    Icons.location_pin,
                    color: PikupColors.deepYellow,
                  ),
                );
              },
            ),
            Divider(),
            BlocBuilder<BookingCubit, BookingState>(
              buildWhen: (previous, current) {
                return (current.event ==
                        BookingEvent.destinationLocationAddressUpdated ||
                    current.event ==
                        BookingEvent.destinationAddressPredictionsUpdated);
              },
              builder: (context, snapshot) {
                return SearchField(
                  controller: _destinationTextController,
                  predictions: Provider.of<BookingCubit>(context)
                      .state
                      .destinationAddressPredictions,
                  onChanged: (val) {
                    final bookingCubit =
                        Provider.of<BookingCubit>(context, listen: false);
                    bookingCubit.getDestinationAddressPredictions(val);
                  },
                  onItemSelected: (FormattedAddress val) {
                    FocusScope.of(context).unfocus();
                    Provider.of<BookingCubit>(context, listen: false)
                        .selectDestinationAddress(val);
                  },
                  hintText: 'Destination location',
                  prefixIcon: Icon(
                    Icons.pin_drop,
                    color: PikupColors.deepYellow,
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }

  void _initializePickupTextField() {
    _pickupTextController.text =
        Provider.of<BookingCubit>(context, listen: false)
            .state
            .currentLocationAddress
            .address;
    _pickupTextController.selection = TextSelection.fromPosition(
      TextPosition(offset: _pickupTextController.text.length),
    );

    _destinationTextController.text =
        Provider.of<BookingCubit>(context, listen: false)
            .state
            .destinationLocationAddress
            .address;
    _destinationTextController.selection = TextSelection.fromPosition(
      TextPosition(offset: _destinationTextController.text.length),
    );
  }

  @override
  void dispose() {
    _pickupTextController.dispose();
    _destinationTextController.dispose();
    super.dispose();
  }
}
