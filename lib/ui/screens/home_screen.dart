import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pikup_customer/bloc/booking_cubit/booking_cubit.dart';
import 'package:pikup_customer/constants/constants.dart';
import 'package:pikup_customer/ui/screens/pick_location_screen.dart';
import 'package:pikup_customer/ui/widgets/geographic_map.dart';
import 'package:pikup_customer/ui/widgets/home_screen_drawer.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pikup_customer/ui/widgets/loading_progress.dart';
import 'package:pikup_customer/ui/widgets/top_side_toolbar.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  static const String routeName = '/home_screen';

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BookingCubit>(
      create: (_) => BookingCubit(),
      child: _HomeScreenWidget(),
    );
  }
}

class _HomeScreenWidget extends StatefulWidget {
  @override
  _HomeScreenWidgetState createState() => _HomeScreenWidgetState();
}

class _HomeScreenWidgetState extends State<_HomeScreenWidget> {
  GoogleMapController? _googleMapController;

  void _onNewEvent(BuildContext context, BookingState state) {
    if (state.event == BookingEvent.loading) {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) => LoadingProgress());
    } else if (state.event == BookingEvent.currentLocationUpdated) {
      _googleMapController?.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(
              state.currentLocation!.latitude,
              state.currentLocation!.longitude,
            ),
            zoom: 14.0,
          ),
        ),
      );
    } else if (state.event == BookingEvent.currentLocationFetchError) {
      ScaffoldMessenger.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            duration: Duration(
              seconds: kSnackBarDuration,
            ),
            content: Text('Error: Unable to access the device location.'),
            action: SnackBarAction(
              label: 'Dismiss',
              onPressed: () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
              },
            ),
          ),
        );
    } else if (state.event == BookingEvent.directionDetailsUpdated) {
      final bookingCubit = Provider.of<BookingCubit>(context, listen: false);
      bookingCubit.updateMarkerSet(_updateMarkers(state));
      bookingCubit.updateCircleSet(_updateCircles(state));
      bookingCubit.decodePolylinePoints();
    } else if (state.event == BookingEvent.drawPolyline) {
      Navigator.of(context).pop();
      _updateLatLngBounds(
        LatLng(state.pickupLocation!.latitude, state.pickupLocation!.longitude),
        LatLng(state.destinationLocation!.latitude, state.destinationLocation!.longitude),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<BookingCubit, BookingState>(
      listener: _onNewEvent,
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 0.0,
        ),
        body: SafeArea(
          child: Stack(
            children: [
              BlocBuilder<BookingCubit, BookingState>(
                  buildWhen: (previousState, currentState) {
                return currentState.event == BookingEvent.drawPolyline;
              }, builder: (context, state) {
                return GeographicMap(
                  onMapCreated: _onMapCreated,
                  polylineSet: state.polylineSet,
                  markerSet: state.markerSet,
                  circleSet: state.circleSet,
                );
              }),
              TopSideToolbar(
                onCurrentPositionPressed: _onCurrentPositionButtonPressed,
                onPickLocationPressed: _onPickLocationButtonPressed,
              ),
            ],
          ),
        ),
        drawer: HomeScreenDrawer(),
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) async {
    _googleMapController = controller;
    final bookingCubit = Provider.of<BookingCubit>(context, listen: false);
    await bookingCubit.setCurrentPosition();
  }

  void _onCurrentPositionButtonPressed() async {
    final bookingCubit = Provider.of<BookingCubit>(context, listen: false);
    await bookingCubit.setCurrentPosition();
  }

  void _onPickLocationButtonPressed() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => BlocProvider.value(
          value: Provider.of<BookingCubit>(context, listen: false),
          child: PickLocationScreen(),
        ),
      ),
    );
  }

  void _updateLatLngBounds(LatLng pickupLatLng, LatLng dropOffLatLng) {
    LatLngBounds latLngBounds;
    if (pickupLatLng.latitude > dropOffLatLng.latitude &&
        pickupLatLng.longitude > dropOffLatLng.longitude) {
      latLngBounds =
          LatLngBounds(southwest: dropOffLatLng, northeast: pickupLatLng);
    } else if (pickupLatLng.longitude > dropOffLatLng.longitude) {
      latLngBounds = LatLngBounds(
        southwest: LatLng(pickupLatLng.latitude, dropOffLatLng.longitude),
        northeast: LatLng(dropOffLatLng.latitude, pickupLatLng.longitude),
      );
    } else if (pickupLatLng.latitude > dropOffLatLng.latitude) {
      latLngBounds = LatLngBounds(
        southwest: LatLng(dropOffLatLng.latitude, pickupLatLng.longitude),
        northeast: LatLng(pickupLatLng.latitude, dropOffLatLng.longitude),
      );
    } else {
      latLngBounds =
          LatLngBounds(southwest: pickupLatLng, northeast: dropOffLatLng);
    }

    _googleMapController
        ?.animateCamera(CameraUpdate.newLatLngBounds(latLngBounds, 70));
  }
  
  Set<Marker> _updateMarkers(BookingState state) {
    final pickupLocationMarker = Marker(
      markerId: MarkerId('pickupMarker'),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
      infoWindow: InfoWindow(title: state.pickupLocationAddress.address, snippet: 'Pickup Location'),
      position: LatLng(state.pickupLocation!.latitude, state.pickupLocation!.longitude),
    );

    final destinationLocationMarker = Marker(
      markerId: MarkerId('destinationMarker'),
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      infoWindow: InfoWindow(title: state.destinationLocationAddress.address, snippet: 'Drop off Location'),
      position: LatLng(state.destinationLocation!.latitude, state.destinationLocation!.longitude),
    );

    return { pickupLocationMarker, destinationLocationMarker };
  }

  Set<Circle> _updateCircles(BookingState state) {
    final pickupCircle = Circle(
      circleId: CircleId('pickupCircle'),
      fillColor: Colors.deepPurple,
      center: LatLng(state.pickupLocation!.latitude, state.pickupLocation!.longitude),
      radius: 12,
      strokeWidth: 4,
      strokeColor: Colors.purple,
    );

    final dropOffCircle = Circle(
      circleId: CircleId('dropOffCircle'),
      fillColor: Colors.deepOrange,
      center: LatLng(state.destinationLocation!.latitude, state.destinationLocation!.longitude),
      radius: 12,
      strokeWidth: 4,
      strokeColor: Colors.orange,
    );

    return { pickupCircle, dropOffCircle };
  }

  @override
  void dispose() {
    _googleMapController?.dispose();
    super.dispose();
  }
}
