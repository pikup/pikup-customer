import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:pikup_customer/bloc/login_cubit/login_cubit.dart';
import 'package:pikup_customer/constants/constants.dart';
import 'package:pikup_customer/ui/screens/otp_screen.dart';
import 'package:pikup_customer/ui/widgets/loading_progress.dart';
import 'package:pikup_customer/ui/widgets/stretched_button.dart';
import 'package:pikup_customer/utility/util.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  static const String routeName = '/login_screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  BuildContext? _dialogContext;

  void _onNewEvent(BuildContext context, LoginState state) {
    if (state.phoneNumberFormStatus == FormzStatus.submissionInProgress) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (buildContext) {
          _dialogContext = buildContext;
          return LoadingProgress();
        },
      );
    } else if (state.phoneNumberFormStatus == FormzStatus.submissionFailure) {
      if (_dialogContext != null) {
        Navigator.of(_dialogContext!).pop();
        _dialogContext = null;
      }

      ScaffoldMessenger.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            duration: Duration(
              seconds: kSnackBarDuration,
            ),
            content: Text('Something went wrong.'),
            action: SnackBarAction(
              label: 'Dismiss',
              onPressed: () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
              },
            ),
          ),
        );
    } else if (state.loginVerificationStatus.verificationStatus ==
        LoginVerificationStatus.Failed) {
      if (_dialogContext != null) {
        Navigator.of(_dialogContext!).pop();
        _dialogContext = null;
      }

      ScaffoldMessenger.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Text(
              state.loginVerificationStatus.message ??
                  'Failed to authenticate.',
            ),
          ),
        );
    } else if (state.loginVerificationStatus.verificationStatus ==
        LoginVerificationStatus.codeSent) {
      if (_dialogContext != null) {
        Navigator.of(_dialogContext!).pop();
        _dialogContext = null;
      }

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (_) => BlocProvider.value(
                  value: context.read<LoginCubit>(),
                  child: OtpScreen(),
                )),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
      ),
      body: BlocListener<LoginCubit, LoginState>(
        listener: _onNewEvent,
        child: LoginForm(),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final String _countryCode = '+91';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Enter your mobile number.',
                    style: TextStyle(
                      fontSize: Util.displayHeight(context) * 0.035,
                      fontWeight: Theme.of(context).textTheme.headline6?.fontWeight,
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Image.asset(
                        'assets/images/indian_flag.png',
                        height: Theme.of(context).textTheme.headline4?.fontSize,
                        width: Theme.of(context).textTheme.headline4?.fontSize,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Text(
                        _countryCode,
                        style: TextStyle(
                          fontSize: Theme.of(context).textTheme.headline6?.fontSize,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        child: _PhoneNumberInput(),
                      ),
                    ],
                  ),
                  const SizedBox(
                    width: 80,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'By continuing you may receive an SMS for verification. Message and data rates may apply.',
                    softWrap: true,
                    style: TextStyle(
                      color: Colors.grey[600],
                      fontSize: Util.displayHeight(context) * 0.02,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  StretchedButton(
                    text: Text('Next'),
                    onPressed: _verifyPhoneNumber,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// This message is responsible for verifying the phoneNumber entered by user.
  void _verifyPhoneNumber() async {
    final loginCubit = Provider.of<LoginCubit>(context, listen: false);
    await loginCubit.verifyPhoneNumber();
  }
}

/// Phone number input field.
class _PhoneNumberInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginCubit, LoginState>(
      buildWhen: (previous, current) =>
          previous.phoneNumber != current.phoneNumber,
      builder: (context, state) {
        return TextFormField(
          onChanged: (phoneNumber) => context
              .read<LoginCubit>()
              .phoneNumberChanged('+91' + phoneNumber),
          keyboardType: TextInputType.phone,
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.headline6?.fontSize,
          ),
          decoration: InputDecoration(
            hintText: 'Mobile number',
            hintStyle: TextStyle(
              fontSize: Theme.of(context).textTheme.headline6?.fontSize,
            ),
            errorText: state.phoneNumber.invalid
                ? 'This phone number is invalid'
                : null,
          ),
          inputFormatters: [
            LengthLimitingTextInputFormatter(10),
          ],
        );
      },
    );
  }
}
