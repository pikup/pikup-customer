import 'package:flutter/material.dart';

// ToDo: Implement user profile screen
class UserProfileScreen extends StatelessWidget {
  static const String routeName = '/user_profile_screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Center(child: Text('User profile screen')),
    );
  }
}
