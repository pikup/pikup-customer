import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:pikup_customer/constants/constants.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';
import 'package:pikup_customer/ui/widgets/loading_progress.dart';
import 'package:pikup_customer/ui/widgets/stretched_button.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pikup_customer/bloc/login_cubit/login_cubit.dart';

class OtpScreen extends StatefulWidget {
  static const String routeName = '/otp_screen';

  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  BuildContext? _dialogContext;

  void _onNewEvent(BuildContext context, LoginState state) {
    if (state.smsCodeFormStatus == FormzStatus.submissionInProgress) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (buildContext) {
          _dialogContext = buildContext;
          return LoadingProgress();
        },
      );
    } else if (state.smsCodeFormStatus == FormzStatus.submissionFailure) {
      if (_dialogContext != null) {
        Navigator.of(_dialogContext!).pop();
        _dialogContext = null;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final phoneNumber = context.read<LoginCubit>().state.phoneNumber.value;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
      ),
      body: BlocListener<LoginCubit, LoginState>(
        listener: _onNewEvent,
        child: SafeArea(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
            child: Stack(
              children: <Widget>[
                ListView(
                  children: [
                    Text(
                      'Enter the 6-digit code sent to you at $phoneNumber.',
                      style: TextStyle(
                        fontSize: Theme.of(context).textTheme.headline5?.fontSize,
                        fontWeight:
                        Theme.of(context).textTheme.headline6?.fontWeight,
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    _OtpField(
                      onChanged: (smsCode) =>
                          context.read<LoginCubit>().smsCodeChanged(smsCode),
                    ),
                    const SizedBox(
                      height: 80,
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: StretchedButton(
                    text: Text(
                      'Next',
                    ),
                    onPressed: _signInManually,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// This method is called when user presses the next button manually
  Future<void> _signInManually() async {
    final loginCubit = context.read<LoginCubit>();
    await loginCubit.signInWithSmsCode();
  }
}

class _OtpField extends StatelessWidget {
  final void Function(String) onChanged;

  _OtpField({required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      appContext: context,
      length: kOtpLength,
      keyboardType: TextInputType.phone,
      autoFocus: true,
      autoDismissKeyboard: false,
      obscureText: true,
      showCursor: false,
      blinkWhenObscuring: true,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(5),
        activeFillColor: Colors.white,
        activeColor: Colors.black,
        inactiveColor: PikupColors.grey,
        selectedColor: Colors.black,
      ),
      onChanged: onChanged,
    );
  }
}
