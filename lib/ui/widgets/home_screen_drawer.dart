import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pikup_customer/bloc/authentication_bloc/authentication_bloc.dart';
import 'package:pikup_customer/ui/screens/user_profile_screen.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';
import 'package:pikup_customer/ui/widgets/drawer_list_tile.dart';
import 'package:provider/provider.dart';

import 'loading_progress.dart';

class HomeScreenDrawer extends StatefulWidget {
  @override
  _HomeScreenDrawerState createState() => _HomeScreenDrawerState();
}

class _HomeScreenDrawerState extends State<HomeScreenDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          Header(),
          DrawerListTile(
            title: 'About',
            leadingIcon: Icons.info,
          ),
          DrawerListTile(
            title: 'Logout',
            leadingIcon: Icons.logout,
            onTap: _logOut,
          ),
        ],
      ),
    );
  }

  void _logOut() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return LoadingProgress();
      },
    );
    final authenticationBloc =
        Provider.of<AuthenticationBloc>(context, listen: false);
    authenticationBloc.add(AuthenticationLogoutRequested());
  }
}

/// Header widget of the drawer
class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, UserProfileScreen.routeName);
        },
        child: DrawerHeader(
          decoration: BoxDecoration(
            color: PikupColors.primaryColor,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              CircleAvatar(
                radius: 40.0,
                backgroundColor: Colors.white,
                child: Icon(
                  Icons.person_rounded,
                  color: PikupColors.primaryColor,
                  size: 50,
                ),
              ),
              SizedBox(
                width: 16.0,
              ),
              BlocBuilder<AuthenticationBloc, AuthenticationState>(
                builder: (context, state) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        state.user.displayName ?? 'Unknown',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 16.0,
                        ),
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Text(
                        state.user.phoneNumber ?? '',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  );
                },
              ),
              SizedBox(
                width: 16.0,
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
