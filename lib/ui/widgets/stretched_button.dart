import 'package:flutter/material.dart';
import 'package:pikup_customer/utility/util.dart';

class StretchedButton extends StatelessWidget {
  final Text text;
  final Color? backgroundColor;
  final void Function() onPressed;

  StretchedButton({
    required this.text,
    required this.onPressed,
    this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        child: text,
        style: TextButton.styleFrom(
          primary: Colors.white,
          backgroundColor: backgroundColor ?? Colors.black,
        ),
        onPressed: this.onPressed,
      ),
    );
  }
}
