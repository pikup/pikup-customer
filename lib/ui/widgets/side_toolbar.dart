import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pikup_customer/ui/widgets/current_position_button.dart';
import 'package:pikup_customer/ui/widgets/pick_location_button.dart';

class SideToolbar extends StatelessWidget {
  final void Function() onPickLocationPressed;
  final void Function() onCurrentPositionPressed;

  const SideToolbar({
    required this.onPickLocationPressed,
    required this.onCurrentPositionPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          PickLocationButton(
            onPressed: onPickLocationPressed,
          ),
          SizedBox(
            height: 16.0,
          ),
          CurrentPositionButton(
            onPressed: onCurrentPositionPressed,
          )
        ],
      ),
    );
  }
}
