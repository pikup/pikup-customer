import 'package:flutter/material.dart';
import 'package:rest_api_repository/geocoding_repository.dart';

class SearchField extends StatelessWidget {
  final TextEditingController controller;
  final bool autoFocus;
  final List<FormattedAddress> predictions;
  final Icon? prefixIcon;
  final String hintText;
  final TextInputType keyboardType;
  final bool enableSuggestions;
  final void Function(String)? onChanged;
  final void Function(FormattedAddress)? onItemSelected;

  SearchField({
    required this.controller,
    this.autoFocus = false,
    required this.hintText,
    this.predictions = const <FormattedAddress>[],
    this.prefixIcon,
    this.keyboardType = TextInputType.text,
    this.enableSuggestions = true,
    this.onChanged,
    this.onItemSelected,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          controller: controller,
          autofocus: autoFocus,
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            prefixIcon: prefixIcon,
            labelText: hintText,
            border: OutlineInputBorder(),
          ),
          keyboardType: keyboardType,
          enableSuggestions: enableSuggestions,
          onChanged: onChanged,
        ),
        (predictions.isEmpty)
            ? Container()
            : Container(
                margin: EdgeInsets.symmetric(horizontal: 2.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(2.5),
                ),
                child: ListView.separated(
                  shrinkWrap: true,
                  itemCount: predictions.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      leading: Icon(Icons.location_city),
                      title: Text(
                        predictions[index].address,
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      onTap: () {
                        controller.text = predictions[index].address;
                        controller.selection = TextSelection.fromPosition(
                          TextPosition(offset: controller.text.length),
                        );

                        if (onItemSelected != null) {
                          onItemSelected!(predictions[index]);
                        }
                      },
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                ),
              ),
      ],
    );
  }
}
