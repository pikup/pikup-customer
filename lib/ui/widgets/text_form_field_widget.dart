import 'package:flutter/material.dart';

class TextFormFieldWidget extends StatelessWidget {
  final Icon? prefixIcon;
  final String labelText;
  final TextInputType keyboardType;
  final bool enableSuggestions;
  final void Function(String)? onChanged;
  final String? Function(String?)? validator;
  final String? errorText;

  TextFormFieldWidget({
    required this.labelText,
    this.prefixIcon,
    this.keyboardType = TextInputType.text,
    this.enableSuggestions = true,
    this.onChanged,
    this.validator,
    this.errorText,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofocus: true,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        prefixIcon: prefixIcon,
        labelText: labelText,
        alignLabelWithHint: true,
        border: OutlineInputBorder(),
        errorText: errorText,
      ),

      keyboardType: keyboardType,
      enableSuggestions: enableSuggestions,
      onChanged: onChanged,
      validator: validator,
    );
  }
}