import 'package:flutter/material.dart';
import 'package:pikup_customer/ui/widgets/side_toolbar.dart';

class TopSideToolbar extends StatelessWidget {
  final void Function() onPickLocationPressed;
  final void Function() onCurrentPositionPressed;

  const TopSideToolbar({
    required this.onCurrentPositionPressed,
    required this.onPickLocationPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(2.5),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 10.0,
                spreadRadius: 1.0,
              )
            ],
          ),
          margin: EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 8.0,
          ),
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              /// Drawer menu button
              GestureDetector(
                onTap: () {
                  Scaffold.of(context).openDrawer();
                },
                child: Icon(
                  Icons.menu,
                  color: Colors.black,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              TitleField(),
              SizedBox(
                width: 10,
              ),

              /// Notifications button
              Icon(
                Icons.notifications,
                color: Colors.black,
              ),
            ],
          ),
        ),
        SideToolbar(
          onCurrentPositionPressed: onCurrentPositionPressed,
          onPickLocationPressed: onPickLocationPressed,
        ),
      ],
    );
  }
}

class TitleField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 14),
        child: Text(
          'Pikup',
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
