import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pikup_customer/constants/constants.dart';

class GeographicMap extends StatelessWidget {
  final Function(GoogleMapController controller) onMapCreated;
  final Set<Polyline> polylineSet;
  final Set<Marker> markerSet;
  final Set<Circle> circleSet;

  const GeographicMap({
    required this.onMapCreated,
    required this.polylineSet,
    required this.markerSet,
    required this.circleSet,
  });

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: kInitialCameraPosition,
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      zoomControlsEnabled: false,
      tiltGesturesEnabled: false,
      onMapCreated: (controller) async {
        controller.setMapStyle(await rootBundle.loadString('assets/map_styles/pikup_map_style.json'));
        onMapCreated(controller);
      },
      polylines: polylineSet,
      markers: markerSet,
      circles: circleSet,
      cameraTargetBounds: CameraTargetBounds(
        kIndianMapBounds,
      ),
    );
  }
}
