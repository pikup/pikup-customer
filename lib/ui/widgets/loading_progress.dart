import 'package:flutter/material.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';

class LoadingProgress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        backgroundColor: PikupColors.deepYellow,
      ),
    );
  }
}
