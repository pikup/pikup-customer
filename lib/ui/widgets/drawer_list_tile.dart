import 'package:flutter/material.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';

class DrawerListTile extends StatelessWidget {
  final String title;
  final IconData leadingIcon;
  final void Function()? onTap;

  DrawerListTile({
    required this.title,
    required this.leadingIcon,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        leadingIcon,
        color: PikupColors.primaryColor,
      ),
      title: Text(
        title,
      ),
      onTap: onTap,
    );
  }
}
