import 'package:flutter/material.dart';

class CurrentPositionButton extends StatelessWidget {
  final void Function() onPressed;

  const CurrentPositionButton({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(2.5),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 10.0,
            spreadRadius: 1.0,
          )
        ],
      ),
      child: FloatingActionButton(
        heroTag: 'currentPositionButton',
        backgroundColor: Colors.white,
        child: Icon(
          Icons.my_location,
          color: Colors.black,
          size: 30.0,
        ),
        onPressed: onPressed,
      ),
    );
  }
}
