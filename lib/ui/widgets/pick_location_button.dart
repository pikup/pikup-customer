import 'package:flutter/material.dart';

class PickLocationButton extends StatelessWidget {
  final void Function() onPressed;

  const PickLocationButton({required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(2.5),
      boxShadow: [
        BoxShadow(
          color: Colors.grey,
          blurRadius: 10.0,
          spreadRadius: 1.0,
        )
      ],
    ),
      child: FloatingActionButton(
        heroTag: 'pickLocationButton',
        backgroundColor: Colors.white,
        child: Icon(
          Icons.add_location_alt,
          color: Colors.black,
          size: 30.0,
        ),
        onPressed: onPressed,
      ),
    );
  }
}