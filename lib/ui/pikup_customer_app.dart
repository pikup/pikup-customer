import 'package:firestore_repository/firestore_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pikup_customer/bloc/authentication_bloc/authentication_bloc.dart';
import 'package:pikup_customer/bloc/login_cubit/login_cubit.dart';
import 'package:pikup_customer/router.dart' as routes;
import 'package:pikup_customer/ui/screens/home_screen.dart';
import 'package:pikup_customer/ui/screens/login_screen.dart';
import 'package:pikup_customer/ui/screens/user_info_form_screen.dart';
import 'package:pikup_customer/ui/theme/pikup_colors.dart';
import 'package:pikup_customer/ui/theme/pikup_design_theme.dart';
import 'package:authentication_repository/authentication_repository.dart';

class PikupCustomerApp extends StatelessWidget {
  final AuthenticationRepository authenticationRepository;
  final FirestoreUserRepository userRepository;

  PikupCustomerApp({
    Key? key,
    required this.authenticationRepository,
    required this.userRepository,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<AuthenticationRepository>.value(
          value: authenticationRepository,
        ),
        RepositoryProvider<FirestoreUserRepository>.value(
          value: userRepository,
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(
            create: (_) => AuthenticationBloc(
              authenticationRepository: authenticationRepository,
            ),
          ),
        ],
        child: AppView(),
      ),
    );
  }
}

class AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState? get _navigator => _navigatorKey.currentState;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: _navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: PikupDesignTheme.lightTheme,
      onGenerateRoute: routes.Router.generateRoute,
      builder: (context, child) {
        return BlocListener<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            switch (state.status) {
              case AuthenticationStatus.authenticated:
                if (state.user.displayName?.isEmpty ?? true) {
                  _navigator?.pushNamedAndRemoveUntil<void>(
                    UserInfoFormScreen.routeName,
                    (route) => false,
                  );
                } else {
                  _navigator?.pushNamedAndRemoveUntil<void>(
                    HomeScreen.routeName,
                    (route) => false,
                  );
                }
                break;
              case AuthenticationStatus.unauthenticated:
                _navigator?.pushAndRemoveUntil<void>(
                  MaterialPageRoute(
                      builder: (_) => BlocProvider<LoginCubit>(
                            create: (_) => LoginCubit(
                                context.read<AuthenticationRepository>()),
                            child: LoginScreen(),
                          )),
                  (route) => false,
                );
                break;
              default:
                break;
            }
          },
          child: child,
        );
      },
    );
  }
}
