import 'package:google_maps_flutter/google_maps_flutter.dart';

const kOtpLength = 6;
const kSnackBarDuration = 20;

// Initial camera position at top of Indian subcontinent
const kInitialCameraPosition = CameraPosition(
  target: LatLng(20.5937, 82.9629),
  zoom: 4.0,
);

final kIndianMapBounds = LatLngBounds(
  southwest: LatLng(7.798000, 68.14712),
  northeast: LatLng(37.090000, 97.34466),
);