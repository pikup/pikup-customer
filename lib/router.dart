import 'package:flutter/material.dart';
import 'package:pikup_customer/ui/screens/error_screen.dart';
import 'package:pikup_customer/ui/screens/home_screen.dart';
import 'package:pikup_customer/ui/screens/loading_screen.dart';
import 'package:pikup_customer/ui/screens/login_screen.dart';
import 'package:pikup_customer/ui/screens/otp_screen.dart';
import 'package:pikup_customer/ui/screens/pick_location_screen.dart';
import 'package:pikup_customer/ui/screens/splash_screen.dart';
import 'package:pikup_customer/ui/screens/user_info_form_screen.dart';
import 'package:pikup_customer/ui/screens/user_profile_screen.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case SplashScreen.route:
        return MaterialPageRoute(
          builder: (_) => SplashScreen(),
        );
      case LoginScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => LoginScreen(),
        );
      case OtpScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => OtpScreen(
          ),
        );
      case HomeScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => HomeScreen(),
        );
      case LoadingScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => LoadingScreen(),
        );
      case ErrorScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => ErrorScreen(),
        );
      case UserInfoFormScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => UserInfoFormScreen(),
        );
      case UserProfileScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => UserProfileScreen(),
        );
      case PickLocationScreen.routeName:
        return MaterialPageRoute(
          builder: (_) => PickLocationScreen()
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text(
                'There doesn\'t exist any route with the name ${routeSettings.name}',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
          ),
        );
    }
  }
}
