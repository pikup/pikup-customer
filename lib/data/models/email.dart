import 'package:formz/formz.dart';
import 'package:pikup_customer/utility/util.dart';

enum EmailValidationError { invalid }

class Email extends FormzInput<String, EmailValidationError> {
  const Email.pure() : super.pure('');
  const Email.dirty([String value = '']) : super.dirty(value);

  @override
  EmailValidationError? validator(String? value) {
    return (value == null || value.isEmpty || Util.isValidEmail(value))
        ? null
        : EmailValidationError.invalid;
  }
}
