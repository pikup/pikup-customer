import 'package:formz/formz.dart';

enum SmsCodeValidationError { invalid }

class SmsCode extends FormzInput<String, SmsCodeValidationError> {
  const SmsCode.pure() : super.pure('');
  const SmsCode.dirty([String value = '']) : super.dirty(value);

  @override
  SmsCodeValidationError? validator(String? value) {
    return (value?.length != 6) ? SmsCodeValidationError.invalid : null;
  }
}