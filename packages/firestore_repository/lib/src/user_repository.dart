import 'package:firestore_repository/src/models/firestore_user.dart';

abstract class UserRepository {
  Future<void> addCustomer({
    required String id,
    String? name,
    String? phoneNumber,
    String? email,
    bool? isEmailVerified,
  });

  Future<void> deleteCustomer(String id);

  Future<void> updateCustomer({
    required String id,
    String? name,
    String? phoneNumber,
    String? email,
    bool? isEmailVerified,
  });

  Stream<FirestoreUser> firestoreUser(String userId);
}
