import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class FirestoreUserEntity extends Equatable {
  /// Constant field names
  static const kId = 'id';
  static const kName = 'name';
  static const kPhoneNumber = 'phoneNumber';
  static const kEmail = 'email';
  static const kIsEmailVerified = 'isEmailVerified';

  /// Member variables
  final String? id;
  final String? name;
  final String? phoneNumber;
  final String? email;
  final bool? isEmailVerified;

  const FirestoreUserEntity({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isEmailVerified,
  });

  @override
  List<Object?> get props => [id, name, phoneNumber, email, isEmailVerified];

  static FirestoreUserEntity fromSnapshot(DocumentSnapshot snap) {
    final Map<String, dynamic> json = snap.data() as Map<String, dynamic>;
    return FirestoreUserEntity(
      id: json[kId] as String,
      name: json[kName] as String,
      phoneNumber: json[kPhoneNumber] as String,
      email: json[kEmail] as String,
      isEmailVerified: json[kIsEmailVerified] as bool,
    );
  }

  Map<String, dynamic> toDocument() {
    return <String, dynamic>{
      kName: name,
      kPhoneNumber: phoneNumber,
      kEmail: email,
      kIsEmailVerified: isEmailVerified
    };
  }
}
