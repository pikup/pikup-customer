import 'package:flutter/material.dart';
import '../entities/firestore_user_entity.dart';

@immutable
class FirestoreUser {
  final String? id;
  final String? name;
  final String? phoneNumber;
  final String? email;
  final bool? isEmailVerified;

  const FirestoreUser({
    this.id,
    this.name,
    this.phoneNumber,
    this.email,
    this.isEmailVerified,
  });

  FirestoreUser copyWith({
    String? id,
    String? name,
    String? phoneNumber,
    String? email,
    bool? isEmailVerified,
  }) {
    return FirestoreUser(
        id: id ?? this.id,
        name: name ?? this.name,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        email: email ?? this.email,
        isEmailVerified: isEmailVerified ?? this.isEmailVerified);
  }

  FirestoreUserEntity toEntity() {
    return FirestoreUserEntity(
      id: id,
      name: name,
      phoneNumber: phoneNumber,
      email: email,
      isEmailVerified: isEmailVerified,
    );
  }

  static FirestoreUser fromEntity(FirestoreUserEntity entity) {
    return FirestoreUser(
      id: entity.id,
      name: entity.name,
      phoneNumber: entity.phoneNumber,
      email: entity.email,
      isEmailVerified: entity.isEmailVerified,
    );
  }

  @override
  int get hashCode =>
      id.hashCode ^
      name.hashCode ^
      phoneNumber.hashCode ^
      email.hashCode ^
      isEmailVerified.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FirestoreUser &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          phoneNumber == other.phoneNumber &&
          email == other.email &&
          isEmailVerified == other.isEmailVerified;

  static const empty = FirestoreUser();
}
