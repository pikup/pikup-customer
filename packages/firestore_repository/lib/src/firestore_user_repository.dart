import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firestore_repository/src/entities/firestore_user_entity.dart';
import 'package:firestore_repository/src/models/firestore_user.dart';
import 'package:firestore_repository/src/user_repository.dart';

/// Thrown during the first time customer creation on firestore process if a failure occurs
class AddFirestoreCustomerFailure implements Exception {}

/// Thrown during the customer deletion process if a failure occurs
class DeleteFirestoreCustomerFailure implements Exception {}

/// Thrown during the customer data update process if a failure occurs
class UpdateFirestoreCustomerFailure implements Exception {}

class FirestoreUserRepository implements UserRepository {
  final customerCollection = FirebaseFirestore.instance.collection('customers');

  @override
  Stream<FirestoreUser> firestoreUser(String customerId) {
    return customerCollection.doc(customerId).snapshots().map((doc) {
      return doc.exists
          ? FirestoreUser.fromEntity(FirestoreUserEntity.fromSnapshot(doc))
          : FirestoreUser.empty;
    });
  }

  @override
  Future<void> addCustomer({
    required String id,
    String? name,
    String? phoneNumber,
    String? email,
    bool? isEmailVerified,
  }) async {
    final user = FirestoreUser(
      id: id,
      name: name,
      phoneNumber: phoneNumber,
      email: email,
      isEmailVerified: isEmailVerified,
    );
    try {
      await customerCollection
          .doc(id)
          .set(user.toEntity().toDocument())
          .catchError((error) {
        print('Failed to add customer: $error');
      });
    } on Exception {
      throw AddFirestoreCustomerFailure();
    }
  }

  @override
  Future<void> deleteCustomer(String id) async {
    try {
      await customerCollection.doc(id).delete().catchError((error) {
        print('Failed to delete customer: $error');
      });
    } on Exception {
      throw DeleteFirestoreCustomerFailure();
    }
  }

  @override
  Future<void> updateCustomer({
    required String id,
    String? name,
    String? phoneNumber,
    String? email,
    bool? isEmailVerified,
  }) async {
    final map = <String, dynamic>{};

    if (name != null) {
      map.putIfAbsent(FirestoreUserEntity.kName, () => name);
    }

    if (phoneNumber != null) {
      map.putIfAbsent(FirestoreUserEntity.kPhoneNumber, () => phoneNumber);
    }

    if (email != null) {
      map.putIfAbsent(FirestoreUserEntity.kEmail, () => email);
    }

    if (isEmailVerified != null) {
      map.putIfAbsent(
          FirestoreUserEntity.kIsEmailVerified, () => isEmailVerified);
    }

    try {
      await customerCollection.doc(id).update(map).catchError((error) {
        print('Failed to update customer info: $error');
      });
    } on Exception {
      throw UpdateFirestoreCustomerFailure();
    }
  }
}
