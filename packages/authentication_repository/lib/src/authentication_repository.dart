import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;

import 'models/user.dart';

/// Thrown during the signup or login process if a failure occurs
class PhoneVerificationFailure implements Exception {}

/// Thrown during the logout process if a failure occurs
class LogOutFailure implements Exception {}

/// Thrown during the update of User info process if a failure occurs
class UserInfoUpdateFailure implements Exception {}

/// Thrown during fetching the current logged in user in firebase
class CurrentUserFetchFailure implements Exception {}

/// Repository which manages user authentication.
class AuthenticationRepository {
  final firebase_auth.FirebaseAuth _firebaseAuth;

  AuthenticationRepository({firebase_auth.FirebaseAuth? firebaseAuth})
      : _firebaseAuth = firebaseAuth ?? firebase_auth.FirebaseAuth.instance;

  /// Stream of [User] which will emit the current user when
  /// the authentication state changes.
  ///
  /// Emits [null] if the user is not authenticated.
  Stream<User> get user {
    return _firebaseAuth.authStateChanges().map((firebaseUser) {
      return firebaseUser == null ? User.empty : firebaseUser.toUser;
    });
  }

  /// Stream of [User] which will emit the updated data of user when
  /// the user data changes.
  ///
  /// Emits [null] if the user is not authenticated.
  Stream<User> get updatedUser {
    return _firebaseAuth.userChanges().map((firebaseUser) {
      return firebaseUser == null ? User.empty : firebaseUser.toUser;
    });
  }

  /// Creates a new user or verifies the existing user with the provided [phoneNumber].
  ///
  /// Throws a [PhoneVerificationFailure] if an exception occurs.
  Future<void> verifyPhoneNumber({
    required String phoneNumber,
    required void Function(firebase_auth.PhoneAuthCredential)
        verificationCompleted,
    required void Function(firebase_auth.FirebaseAuthException)
        verificationFailed,
    required void Function(String, int?) codeSent,
    required void Function(String) codeAutoRetrievalTimeout,
  }) async {
    try {
      await _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
      );
    } on Exception {
      throw PhoneVerificationFailure();
    }
  }

  /// This method is used to sign the user in (or link) with the credential
  Future<void> signInWithCredential(
      firebase_auth.PhoneAuthCredential phoneAuthCredential) async {
    await _firebaseAuth.signInWithCredential(phoneAuthCredential);
  }

  /// This method is used to sign the user in with [smsCode] and [verificationId]
  Future<void> signInWithSmsCode(
      {required String verificationId, required String smsCode}) async {
    firebase_auth.PhoneAuthCredential phoneAuthCredential =
        firebase_auth.PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: smsCode,
    ) as firebase_auth.PhoneAuthCredential;
    await signInWithCredential(phoneAuthCredential);
  }

  /// This method is used to signOut the user from firebase.
  Future<void> logOut() async {
    try {
      await _firebaseAuth.signOut();
    } on Exception {
      throw LogOutFailure();
    }
  }

  /// This method updates the user email and name.
  Future<void> updateUserInfo({String? displayName, String? email,}) async {
    try {
      if (displayName != null) {
        await _firebaseAuth.currentUser?.updateProfile(displayName: displayName,);
      }

      if (email != null) {
        await _firebaseAuth.currentUser?.updateEmail(email);
      }

      await _firebaseAuth.currentUser?.reload();
    } on Exception {
      throw UserInfoUpdateFailure();
    }
  }

  Future<User> getCurrentUser() async {
    User user = User.empty;
    try {
      if (_firebaseAuth.currentUser?.toUser != null) {
        user = _firebaseAuth.currentUser!.toUser;
      }
    } on Exception {
      throw CurrentUserFetchFailure();
    }

    return user;
  }
}

extension on firebase_auth.User {
  User get toUser {
    return User(
        phoneNumber: phoneNumber,
        uid: uid,
        displayName: displayName,
        email: email);
  }
}
