import 'package:equatable/equatable.dart';

/// User model
///
/// [User.empty] represents an unauthenticated user.
class User extends Equatable {
  const User({
    required this.phoneNumber,
    required this.uid,
    required this.displayName,
    required this.email,
  });

  /// The current user's id.
  final String uid;

  /// The current user's phone number.
  final String? phoneNumber;

  /// The current user's name.
  final String? displayName;

  /// The current user's email address.
  final String? email;

  /// Empty user which represents an unauthenticated user.
  static const empty =
      User(phoneNumber: '', uid: '', displayName: null, email: null);

  @override
  List<Object?> get props => [uid, phoneNumber, displayName, email];
}
