import 'dart:convert';
import '../constants/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

class PlacesServiceException implements Exception {}

class PlacesService {
  final String _authority = 'maps.googleapis.com';
  final String _unencodedAutocompletePath = '/maps/api/place/autocomplete/json';
  final String _unencodedPlaceDetailsPath = '/maps/api/place/details/json';

  Future<Map<String, dynamic>?> autoComplete({
    required String input,
  }) async {
    String apiKey;
    if (Platform.isAndroid) {
      apiKey = androidApiKey;
    } else if (Platform.isIOS) {
      apiKey = iosApiKey;
    } else {
      return null;
    }

    final params = <String, String>{
      'input': input,
      'key': apiKey,
      'components': 'country:in',
      'radius': searchRadius.toString(),
    };

    try {
      http.Response response = await http
          .get(Uri.https(_authority, _unencodedAutocompletePath, params));

      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        if (json['status'] == 'OK') {
          return json;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } on Exception {
      throw PlacesServiceException();
    }
  }

  Future<Map<String, dynamic>?> getPlaceDetails({
    required String placeId,
  }) async {
    String apiKey;
    if (Platform.isAndroid) {
      apiKey = androidApiKey;
    } else if (Platform.isIOS) {
      apiKey = iosApiKey;
    } else {
      return null;
    }

    final params = <String, String>{
      'place_id': placeId,
      'key': apiKey,
    };

    try {
      http.Response response = await http
          .get(Uri.https(_authority, _unencodedPlaceDetailsPath, params));

      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        if (json['status'] == 'OK') {
          return json;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } on Exception {
      throw PlacesServiceException();
    }
  }
}
