
import '../constants/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io' show Platform;

class GeocodingServiceException implements Exception {}

class GeocodingService {
  final String _authority = 'maps.googleapis.com';
  final String _unencodedPath = '/maps/api/geocode/json';

  Future<Map<String, dynamic>?> reverseGeocode({
    required double latitude,
    required double longitude,
  }) async {
    String apiKey;
    if (Platform.isAndroid) {
      apiKey = androidApiKey;
    } else if (Platform.isIOS) {
      apiKey = iosApiKey;
    } else {
      return null;
    }

    Map<String, String> params = {
      'latlng': '$latitude,$longitude',
      'key': apiKey,
    };

    try {
      http.Response response = await http.get(Uri.https(_authority, _unencodedPath, params));

      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        return json;
      } else {
        return null;
      }
    } on Exception {
      throw GeocodingServiceException();
    }
  }
}
