import 'dart:convert';
import '../../geocoding_repository.dart';
import '../constants/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

class DirectionsServiceException implements Exception {}

class DirectionsService {
  final String _authority = 'maps.googleapis.com';
  final String _unencodedDirectionsPath = '/maps/api/directions/json';

  Future<Map<String, dynamic>?> getDirections({
    required LatLngPosition initialPosition,
    required LatLngPosition finalPosition,
  }) async {
    String apiKey;
    if (Platform.isAndroid) {
      apiKey = androidApiKey;
    } else if (Platform.isIOS) {
      apiKey = iosApiKey;
    } else {
      return null;
    }

    final params = <String, String>{
      'origin': '${initialPosition.latitude},${initialPosition.longitude}',
      'destination': '${finalPosition.latitude},${finalPosition.longitude}',
      'key': apiKey,
    };

    try {
      http.Response response = await http
          .get(Uri.https(_authority, _unencodedDirectionsPath, params));

      if (response.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(response.body);
        if (json['status'] == 'OK') {
          return json;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } on Exception {
      throw DirectionsServiceException();
    }
  }
}