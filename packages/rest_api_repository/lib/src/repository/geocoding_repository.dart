import '../services/geocoding_service.dart';
import '../models/formatted_address.dart';

class GeocodingRepository {
  final GeocodingService _geocodingService = GeocodingService();

  Future<FormattedAddress> reverseGeocode({
    required double latitude,
    required double longitude,
  }) async {
    try {
      final result = await _geocodingService.reverseGeocode(
        latitude: latitude,
        longitude: longitude,
      );

      if (result != null) {
        final address = FormattedAddress.fromGeocodeJson(result);
        return address;
      }
    } on Exception {
      throw GeocodingServiceException();
    }

    return FormattedAddress.empty;
  }
}
