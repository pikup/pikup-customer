import '../services/places_service.dart';
import '../../geocoding_repository.dart';
import '../models/formatted_address.dart';

class PlacesRepository {
  final int _listCount = 3;
  final PlacesService _placesService = PlacesService();

  Future<List<FormattedAddress>> autoComplete({
    required String input,
  }) async {
    try {
      final result = await _placesService.autoComplete(
        input: input,
      );

      if (result != null) {
        List<FormattedAddress> predictions = [];
        for (int i = 0; i < _listCount; i++) {
          final address = FormattedAddress.fromPlacesJson(result, i);
          predictions.add(address);
        }
        return predictions;
      }
    } on Exception {
      throw PlacesServiceException();
    }

    return const <FormattedAddress>[];
  }

  Future<LatLngPosition?> getPlacePosition({
    required String placeId,
  }) async {
    try {
      final result = await _placesService.getPlaceDetails(
        placeId: placeId,
      );

      if (result != null) {
        return LatLngPosition.fromPlacesDetailsJson(result);
      }
    } on Exception {
      throw PlacesServiceException();
    }
  }
}