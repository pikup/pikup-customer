import '../models/lat_lng_position.dart';
import '../services/directions_service.dart';
import '../models/direction_details.dart';

class DirectionsRepository {
  final DirectionsService _directionsService = DirectionsService();

  Future<DirectionDetails?> getDirectionDetails({
    required LatLngPosition initialPosition,
    required LatLngPosition finalPosition,
  }) async {
    try {
      final result = await _directionsService.getDirections(
        initialPosition: initialPosition,
        finalPosition: finalPosition,
      );

      if (result != null) {
        final directionDetails = DirectionDetails.fromDirectionDetailsJson(result);
        return directionDetails;
      }
    } on Exception {
      throw DirectionsServiceException();
    }
  }
}