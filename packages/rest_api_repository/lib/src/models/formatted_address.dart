import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class FormattedAddress extends Equatable {
  final String address;
  final String placeId;

  const FormattedAddress({
    required this.address,
    required this.placeId,
  });

  static FormattedAddress fromGeocodeJson(Map<String, dynamic> json) {
    return FormattedAddress(
      address: json['results'][0]['formatted_address'],
      placeId: json['results'][0]['place_id'],
    );
  }

  static FormattedAddress fromPlacesJson(Map<String, dynamic> json, int index) {
    return FormattedAddress(
      address: json['predictions'][index]['description'],
      placeId: json['predictions'][index]['place_id'],
    );
  }

  static const empty = FormattedAddress(
    address: '',
    placeId: '',
  );

  bool isEmpty() => address.isEmpty && placeId.isEmpty;

  @override
  List<Object?> get props => [
        address,
        placeId,
      ];
}
