import 'package:equatable/equatable.dart';

class LatLngPosition extends Equatable {
  final double latitude;
  final double longitude;

  const LatLngPosition({
    required this.latitude,
    required this.longitude,
  });

  factory LatLngPosition.fromPlacesDetailsJson(
    Map<String, dynamic> json,
  ) =>
      LatLngPosition(
        latitude: json['result']['geometry']['location']['lat'],
        longitude: json['result']['geometry']['location']['lng'],
      );

  @override
  List<Object?> get props => [
        latitude,
        longitude,
      ];
}
