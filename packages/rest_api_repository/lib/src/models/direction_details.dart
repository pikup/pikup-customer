import 'package:equatable/equatable.dart';

class DirectionDetails extends Equatable {
  final int distanceValue;
  final String distanceText;
  final int durationValue;
  final String durationText;
  final String encodedPoints;

  const DirectionDetails({
    required this.distanceValue,
    required this.distanceText,
    required this.durationValue,
    required this.durationText,
    required this.encodedPoints,
  });

  factory DirectionDetails.fromDirectionDetailsJson(
    Map<String, dynamic> json,
  ) =>
      DirectionDetails(
        distanceValue: json['routes'][0]['legs'][0]['distance']['value'],
        distanceText: json['routes'][0]['legs'][0]['distance']['text'],
        durationValue: json['routes'][0]['legs'][0]['duration']['value'],
        durationText: json['routes'][0]['legs'][0]['duration']['text'],
        encodedPoints: json['routes'][0]['overview_polyline']['points'],
      );

  @override
  List<Object?> get props => [
        distanceValue,
        distanceText,
        durationValue,
        durationText,
        encodedPoints,
      ];
}
